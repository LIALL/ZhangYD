﻿namespace WindowsFormZhangYiDa
{
    partial class FormZyd
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Btnstart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Btnsuspend = new System.Windows.Forms.Button();
            this.TxtScore = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(895, 544);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Btnstart
            // 
            this.Btnstart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btnstart.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btnstart.Location = new System.Drawing.Point(915, 204);
            this.Btnstart.Name = "Btnstart";
            this.Btnstart.Size = new System.Drawing.Size(125, 39);
            this.Btnstart.TabIndex = 1;
            this.Btnstart.TabStop = false;
            this.Btnstart.Text = "开始";
            this.Btnstart.UseVisualStyleBackColor = true;
            this.Btnstart.Click += new System.EventHandler(this.Btnstart_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(921, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "得分：";
            // 
            // Btnsuspend
            // 
            this.Btnsuspend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btnsuspend.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btnsuspend.Location = new System.Drawing.Point(915, 272);
            this.Btnsuspend.Name = "Btnsuspend";
            this.Btnsuspend.Size = new System.Drawing.Size(125, 38);
            this.Btnsuspend.TabIndex = 3;
            this.Btnsuspend.TabStop = false;
            this.Btnsuspend.Text = "暂停";
            this.Btnsuspend.UseVisualStyleBackColor = true;
            this.Btnsuspend.Click += new System.EventHandler(this.Btnsuspend_Click);
            // 
            // TxtScore
            // 
            this.TxtScore.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.TxtScore.AutoSize = true;
            this.TxtScore.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtScore.Location = new System.Drawing.Point(1003, 118);
            this.TxtScore.Name = "TxtScore";
            this.TxtScore.Size = new System.Drawing.Size(16, 16);
            this.TxtScore.TabIndex = 4;
            this.TxtScore.Tag = "score";
            this.TxtScore.Text = "0";
     

            // 
            // FormZyd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 637);
            this.Controls.Add(this.TxtScore);
            this.Controls.Add(this.Btnsuspend);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btnstart);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FormZyd";
            this.Text = "张益达";
            this.Load += new System.EventHandler(this.FormZyd_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormZyd_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Btnstart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btnsuspend;
        private System.Windows.Forms.Label TxtScore;
        private System.Windows.Forms.Timer timer1;
    }
}

